# Stock Market Index Analysis

[TOC]

## Introduction

Standard & Poor's 500 (aka S&P 500; ticker symbols ^GSPC, SPX or $SPX) is an American stock market index based on the largest 500 companies selected by S&P committee.
However, the companies themselves need not be of American origin.
Each company has to meet certain quantifiable criterias in order to be listed there: market capitalization (market cap in short), liquidity (ability to sell securities without causing change in prices of the securities), financial viability, public float (fraction of shares traded publicly), monthly trading volume (number of securities traded) and other factors.

The index is essentially sum of all market caps listed in the index divided by a divisor that is adjusted whenever companies are merged, spinoff'ed, shares repurchased (companies buying shares back, thus reducing public float), shares issued (hence increasing the total number of shares), or other changes that affect the index.
The market cap itself is calculated by multiplying the total number of issued shares (aka number of shares outstanding) with the price of a single share.
Whenever the share is split, it doesn't affect the market cap of a company; therefore it is not considered in divisor adjustment.
The baseline of S&P 500 is set to 10 for the period 1941..1943; currently the index is valued at around 2,070.

There are many exchange-traded funds (ETFs) that try to track $SPX by holding the same stocks that are listed in the index (which are appropriately weighted with ratio of market cap of a company to the total market cap in the index).
Notable examples include $IVV (iShares Core), $SPY (S&P) and $VOO (Vanguard).
However, the S&P index is believed to be a major non-lagging indicator of global economic outlook.

## Objectives

The purpose of this project is to:

* convince the reader that $SPX is indeed a good macroeconomic indicator by comparing it to some other well-known obvious indicators;

* dissect and describe $SPX by sectors;

* try to predict $SPX with various regression models.

The full analysis is in file `project.R`.

## Gathering the Data

We used either Yahoo's (hidden) API [[9]](#markdown-header-references) or Quandl [[10]](#markdown-header-references) routines to gather our data.
The former is hidden in the sense that it is not officially documented: data can be retrieved through HTTP requests where each query key has a special meaning [[1]](#markdown-header-references).
Yahoo's API supports two types of queries:

* daily quotes of a stock/index:

    `http://finance.yahoo.com/d/quotes.csv?s=<ticker symbol>&f=<list of parameters>`
  
  We queried market cap for all stocks in $SPX with parameter key `j1`.
 
* historical daily time series of a stock/index:

    `http://finance.yahoo.com/q/hp?s=<ticker symbol>&a=00&b=19&c=2010&d=02&e=01&f=2011&g=d`
  
  This example queries daily (`d`) historical data of a stock (e.g. MSFT for Microsoft Corp.) from 19th of January 2010 til 1st of March 2011.

Quandl provides R interface to gather the same data.
Historical data contains highest, lowest, open and (adjusted) close values, and volume of a stock in a given time period.

We used both methods to gather our data; we also built an SQL database consisting of the time series of $SPX stocks since 1994.
It should be noted that Yahoo's ToS forbids distributing the data [[2]](#markdown-header-references).
For that reason we won't publish the database but provide a script for it; see `gather-sp500.ipynb`.

List of ticker symbols and corresponding sector information is harvested and subsequently parsed from respective Wikipedia page [[3]](#markdown-header-references); see the script `parseWiki.ipynb`.
More detailed historical information about the stock info is not available by free means (as far as we know).

## Descriptive statistics

#### Treemap of S&P 500

Here is a treemap of the companies included in $SPX split by sector designated by GICS [[3]](#markdown-header-references).
For each company we calculated today's (21/05/2016) 200-day moving average of daily close price, and the same metric a year from now.
The relative difference shows whether the price of a stock has risen on average over the year or not: green and red indicate positive and negative growth, respectively.
From the treemap it seems that the IT sector is the most influential in $SPX, followed by finance and health care sector.

![Treemap](https://goo.gl/1iogos)

#### Top 10 companies

Below is a table of current top 10 companies by market cap and corresponding time series of the close price.
We applied 30-day moving averaging on the prices so that the actual trends can be seen more clearly.

Ticker |     Company                 |                      Sector |   Market cap (in $1 bn USD)  
:------|:---------------------------:|:---------------------------:|:----------------------------:
  AAPL |      Apple Inc.             |      Information Technology |     521.56
 GOOGL |     Google Inc.             |      Information Technology |     495.49
  MSFT |   Microsoft Corp.           |      Information Technology |     397.90
   XOM |     Exxon Mobil Corp.       |                      Energy |     372.12
    FB |      Facebook               |      Information Technology |     335.66
  AMZN |     Amazon.com Inc          |      Consumer Discretionary |     331.60
   JNJ |    Johnson & Johnson        |                 Health Care |     309.83
    GE |     General Electric        |                 Industrials |     271.82
   WFC |      Wells Fargo            |                  Financials |     247.51
     T |      AT&T Inc               | Telecommunications Services |     236.70

![Top10](https://goo.gl/umbdxY)

From the plot we can clearly see the effects of dot-com bubble of 2001 and mortgage crisis of 2008 on various stocks.
We can deduce that a market bubble of any nature will have a negative impact on various companies, regardless of the sector.
For example, the drop in the share prices of all top 10 companies at 2008 is clearly visible, except for $XOM and $FB (the latter hasn't issued any shares at the time), although the cause of this crisis can be tracked down to housing market bubble.

#### Seasonal behavior

Seasonal decomposition of monthly $SPX shows signs of non-stationary seasonality because the trend doesn't follow a clear predictable path.
Currently, the economy is pretty much flatlining on the peak.

![Seasonal decomposition](https://goo.gl/60YDD8)

Seasonal decomposition of the trend (not shown here) also indicates that the remainders (i.e. deviations/,,errors'' from seasonality approximation) are going deeper either side of zero, which hints that next peaks/dips will be higher/deeper than the previous ones, possibly due to globally connected economy.

#### Comparison to other macroeconomic indicators

In the following plot we see $SPX, monthly unemployment rate in the US (data taken from [[11]](#markdown-header-references)) and (monthly) Durable Goods indicator ([[12]](#markdown-header-references)).
The unemployment rate is obviously lagging behind (unemployment peaks while $SPX climbs up after economic meltdown), which means that $SPX is more reactive to changes in economy.
Durable Goods figure is the total value (in USD) of new orders received from manufactures producing durable goods in the previous month [[13]](#markdown-header-references).
The useful life span of such products is three years or more .


![Unemployment and S&P 500](https://goo.gl/6UXpGt)

## Predicting the Future

Our next interest was to predict the closing values of the stock index.
Due to the stochastic nature and unmeasurable randomness of a stock index, it is rather hard to predict the future -- there are hypothesis like the efficient market theory [[4]](#markdown-header-references), which state that it is impossible to beat the market, because stocks are already accurately priced and they incorporate all available information, or the random-walk hypothesis [[5]](#markdown-header-references), which argues that prices follow a random walk process, and therefore they cannot be predicted. 
Fortunately, scientist have not given up the hope to predict the future and in the last decade there has been a lot of research in machine learning about the topic: ANN, RNN, TDNN, AR etc [[6]](#markdown-header-references).

### Approach

The $SPX is non-stationary in nature, meaning its statistical properties like mean or variance are not constant but changing, which makes it highly unpredictable.
Fortunately, we can say that differencing the series makes it stationary (to verify this we conducted augmented Dicker-Fuller test), and therefore we can try predicting it. 
To forecast the time-series, we used multiple models, for example [[7]](#markdown-header-references)[[8]](#markdown-header-references): 

* ARIMA (autoregressive integrated moving average);
* NNET (neural net);
* TBATS;
* SETAR (self-exciting threshold autoregressive).

Although we tried a lot of different models, especially uni-variate ones and even non-linear auto regression models (LSTAR, SETAR, nnetTs etc), we found out that for a long term prediction, none of these provide accurate enough results.
To find out the best and most accurate models, we used mean absolute percentage error (MAPE) and compared the predicted results with the real data.
The results are shown in the following figure.

![Comparison of different models](http://i.imgur.com/rEA6Gae.png)

From the plot above, we can see that the lowest MAPE scores are achieved by the models NNET, NNETS, SETAR & AAR, respectively.
Due to the fact that forecasting stock index accurately is really hard, we chose to predict the future just to see the general trend of the index, whether it will rise or fall, not the actual closing values.
In order to do that, we first got rid of the overall noisiness by working with the trend of $SPX only.
For the training period we chose years 1990 – 2016.
The results of the forecasts are depicted below.

![Forecasting the trend of S&P500](http://i.imgur.com/vEmiNYb.png)

In conclusion, the prediction of our chosen models indicate that $SPX likely starts to fall soon.
NNETS/NNET and AAR predict fast and slow downward trend in $SPX; SETAR falls somewhere in between.
The real test of this analysis is to wait it out ...

## Sources & References

#### Sources

* https://en.wikipedia.org/wiki/S&P_500_Index

#### References

[1] https://greenido.wordpress.com/2009/12/22/yahoo-finance-hidden-api/

[2] https://policies.yahoo.com/us/en/yahoo/terms/utos/

[3] https://en.wikipedia.org/wiki/List_of_S&P_500_companies

[4] Malkiel, Burton G. "Efficient market hypothesis." The World of Economics. Palgrave Macmillan UK, 1991. 211-218.

[5] Malkiel, Burton Gordon. A random walk down Wall Street: including a life-cycle guide to personal investing. WW Norton & Company, 1999.

[6] Nygren, Karl. "Stock prediction–a neural network approach." Master's Thesis, Royal Institute of Technology, KTH, Stockholm (2004).

[7] http://kukuruku.co/hub/r/oil-series-oil-prices-in-r

[8] https://cran.r-project.org/web/packages/forecast/forecast.pdf

[9] https://finance.yahoo.com/

[10] https://www.quandl.com/

[11] US. Bureau of Labor Statistics, Civilian Unemployment Rate [UNRATE], retrieved from FRED, Federal Reserve Bank of St. Louis https://research.stlouisfed.org/fred2/series/UNRATE

[12] US. Bureau of the Census, Manufacturers' New Orders: Durable Goods [DGORDER], retrieved from FRED, Federal Reserve Bank of St. Louis https://research.stlouisfed.org/fred2/series/DGORDER

[13] http://www.investopedia.com/university/releases/durablegoods.asp